#ifndef LAB4_LIST_H
#define LAB4_LIST_H

#include <string.h>

struct ListNode {
    struct ListNode* next;
    char* str;
};

typedef struct ListNode ListNode;

struct List {
    ListNode* head;
    ListNode* end;
};

typedef struct List List;

int createList(List** pList);
int destroyList(List** pList);
int createNode(ListNode** pNode, const char* data, size_t length);
int appendNode(List* list, const char* data, size_t length);
void printList(List* list);

#endif //LAB4_LIST_H