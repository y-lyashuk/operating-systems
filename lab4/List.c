#include "List.h"
#include <stdio.h>
#include <stdlib.h>

int createList(List** pList) {
    *pList = (List*)malloc(sizeof(List));
    if (*pList == NULL) {
        perror("cannot create List");
        return EXIT_FAILURE;
    }

    (*pList)->head = NULL;
    (*pList)->end = NULL;

    return EXIT_SUCCESS;
}

int destroyList(List** pList) {
    if (*pList == NULL) {
        fprintf(stderr, "Error: null pointer passed\n");
        return EXIT_FAILURE;
    }

    ListNode *currentNode = (*pList)->head, *nextNode;
    while (currentNode != NULL) {
        nextNode = currentNode->next;

        free(currentNode->str);
        free(currentNode);

        currentNode = nextNode;
    }

    free(*pList);
    *pList = NULL;

    return EXIT_SUCCESS;
}

int createNode(ListNode** pNode, const char* data, const size_t length) {
    (*pNode) = (ListNode*)malloc(sizeof(ListNode));
    if ((*pNode) == NULL) {
        perror("cannot allocate memory for node");
        return EXIT_FAILURE;
    }

    (*pNode)->str = (char*)malloc(length + 1);
    if ((*pNode)->str == NULL) {
        perror("cannot allocate memory for string");

        free(*pNode);
        return EXIT_FAILURE;
    }

    strncpy((*pNode)->str, data, length);
    (*pNode)->str[length] = '\0';

    (*pNode)->next = NULL;

    return EXIT_SUCCESS;
}

int appendNode(List* list, const char* data, const size_t length) {
    if (list == NULL) {
        fprintf(stderr, "Error: null pointer passed\n");
        return EXIT_FAILURE;
    }

    ListNode* newNode;
    int err = createNode(&newNode, data, length);

    if (err == EXIT_FAILURE) {
        return EXIT_FAILURE;
    }

    if (list->head == NULL) {
        list->head = newNode;
    }
    else {
        list->end->next = newNode;
    }

    list->end = newNode;

    return EXIT_SUCCESS;
}

void printList(List* list) {
    ListNode* current = list->head;

    while (current != NULL) {
        fprintf(stdout, "%s\n", current->str);
        current = current->next;
    }
}