#include <stdio.h>
#include <stdlib.h>
#include "List.h"

#define BUFFER_SIZE 512

#define TRUE 1
#define FALSE 0

int createListOfLines(List* list) {
    char buffer[BUFFER_SIZE];
    char* fgetsResult;
    size_t lineLength;

    int error;

    printf("Enter the sequence of lines. If you want to stop, enter line starting with '.'\n");

    int continueReadingUserInput = TRUE;
    while (continueReadingUserInput) {
        fgetsResult = fgets(buffer, BUFFER_SIZE, stdin);

        if (buffer[0] == '.' || fgetsResult == NULL) {
            continueReadingUserInput = FALSE;
            continue;
        }

        lineLength = strlen(buffer);
        if (buffer[lineLength - 1] != '\n') {
            fprintf(stderr, "line is too long. max length: %d", BUFFER_SIZE - 1);
            return EXIT_FAILURE;
        }

        error = appendNode(list, buffer, lineLength - 1);
        if (error == EXIT_FAILURE) {
            return EXIT_FAILURE;
        }
    }

    return EXIT_SUCCESS;
}

int main(void) {
    int err;

    List* list;
    err = createList(&list);
    if (err == EXIT_FAILURE) {
        return EXIT_FAILURE;
    }

    err = createListOfLines(list);
    if (err == EXIT_FAILURE) {
        return EXIT_FAILURE;
    }

    printf("Your list of lines:\n");
    printList(list);

    destroyList(&list);

    return EXIT_SUCCESS;
}
