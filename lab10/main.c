#include <stdio.h>
#include <stdlib.h>
#include <zconf.h>
#include <sys/wait.h>

#define FORK_ERROR -1
#define WAIT_ERROR -1

#define CHILD_PROCESS 0

#define IDX_OF_FILENAME 1

#define MIN_AMOUNT_OF_ARGUMENTS 2

int waitProcess(void) {
    int status;

    int err = wait(&status);
    if (err == WAIT_ERROR) {
        perror("Wait error");
        return EXIT_FAILURE;
    }

    if (WIFEXITED(status)) {
        printf("Child process terminated normally with code %d\n", WEXITSTATUS(status));
        return EXIT_SUCCESS;
    }
    if (WIFSIGNALED(status)) {
        printf("Child process was terminated due to signal with a number %d\n", WTERMSIG(status));
        return EXIT_SUCCESS;
    }

    fprintf(stderr, "Unknown program status words\n");
    return EXIT_FAILURE;
}

int main(int argc, char** argv) {
    if (argc < MIN_AMOUNT_OF_ARGUMENTS) {
        fprintf(stderr, "Expecting at least one argument: name of program\n");
        return EXIT_FAILURE;
    }

    pid_t forkResult = fork();
    if (forkResult == FORK_ERROR) {
        perror("Error while forking process");
        return EXIT_FAILURE;
    }
    if (forkResult == CHILD_PROCESS) {
        execvp(argv[IDX_OF_FILENAME], &argv[1]);
        perror("Exec error");
        return EXIT_FAILURE;
    }

    int err = waitProcess();
    if (err == EXIT_FAILURE) {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}