#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

#define BUFFER_SIZE 512

#define READ_ERROR -1
#define SEEK_ERROR -1

#define NO_INPUT 0

#define DECIMAL 10

#define EXIT_CRITERION 0

struct lineDescription {
    int length;
    off_t offset;
};

typedef struct lineDescription lineDescription;

int countLinesInFile (int filedesc, int* linesCount) {
    char buffer[BUFFER_SIZE];

    *linesCount = 0;

    int readResult = read(filedesc, buffer, BUFFER_SIZE);
    while (readResult != NO_INPUT) {
        if (readResult == READ_ERROR) {
            perror("Error while counting lines (read error)");
            return EXIT_FAILURE;
        }

        for (int idx = 0; idx < readResult; idx++) {
            if (buffer[idx] == '\n') {
                ++(*linesCount);
            }
        }
        readResult = read(filedesc, buffer, BUFFER_SIZE);
    }

    return EXIT_SUCCESS;
}

int buildDisplacementTable(int filedesc, lineDescription* table, int maxLineNum) {
    char buffer[BUFFER_SIZE];

    int seekResult = lseek(filedesc, 0, SEEK_SET);
    if (seekResult == SEEK_ERROR) {
        perror("Error while analyzing file (cannot set file pointer)");
        return EXIT_FAILURE;
    }

    int currentLine = 0, lengthOfCurrentLine = 0;

    int readResult = read(filedesc, buffer, BUFFER_SIZE);
    while (readResult != NO_INPUT) {
        if (readResult == READ_ERROR) {
            perror("Error while analyzing file (read error)");
            return EXIT_FAILURE;
        }

        for (int idx = 0; idx < readResult; ++idx) {
            if (buffer[idx] == '\n') {
                table[currentLine].length = lengthOfCurrentLine;

                ++currentLine;
                lengthOfCurrentLine = 0;

                continue;
            }
            ++lengthOfCurrentLine;
        }

        readResult = read(filedesc, buffer, BUFFER_SIZE);
    }

    table[0].offset = 0;
    for (int idx = 1; idx < maxLineNum; ++idx) {
        table[idx].offset = table[idx - 1].offset + table[idx - 1].length + 1;
    }

    return EXIT_SUCCESS;
}

int printRequestedAmountOfBytes(int filedesc, int bytesToRead) {
    char buffer[BUFFER_SIZE];

    int readResult;
    while (bytesToRead > 0) {
        readResult = read(filedesc, buffer, (bytesToRead > BUFFER_SIZE) ? BUFFER_SIZE : bytesToRead);
        if (readResult == READ_ERROR) {
            perror("Cannot print line (read error)");
            return EXIT_FAILURE;
        }

        bytesToRead -= readResult;
        printf("%.*s", readResult, buffer);
    }
    printf("\n");

    return EXIT_SUCCESS;
}

int getNumber(long* number) {
    char numberOfLineBuffer[BUFFER_SIZE], *endOfNumber;

    int readResult = read(STDIN_FILENO, numberOfLineBuffer, BUFFER_SIZE);
    if (readResult == READ_ERROR) {
        perror("Cannot get number of line (read error)");
        return EXIT_FAILURE;
    }
    if (readResult == NO_INPUT) {
        fprintf(stderr, "Cannot read number of line");
        return EXIT_FAILURE;
    }
    if (numberOfLineBuffer[readResult - 1] != '\n') {
        fprintf(stderr, "Entered string is too long. Max length: %d", BUFFER_SIZE);
        return EXIT_FAILURE;
    }

    *number = strtol(numberOfLineBuffer, &endOfNumber, DECIMAL);
    if (errno == ERANGE) {
        perror("Incorrect input: number is out of range");
        return EXIT_FAILURE;
    }
    if (endOfNumber != numberOfLineBuffer + (readResult - 1)) {
        fprintf(stderr, "Incorrect input: one integer number expected\n");
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

int printLines(int filedesc, lineDescription* table, int maxLineNum) {
    long numberOfLine;
    int error;
    do {
        printf("Enter number of line in range [1, %d]. Enter 0 to stop program.\n", maxLineNum);

        error = getNumber(&numberOfLine);
        if (error == EXIT_FAILURE) {
            return EXIT_FAILURE;
        }

        if (numberOfLine < 0 || numberOfLine > maxLineNum) {
            fprintf(stderr, "Expecting number in range [1, %d]\n", maxLineNum);
            return EXIT_FAILURE;
        }
        if (numberOfLine == 0) {
            continue;
        }

        error = lseek(filedesc, table[numberOfLine - 1].offset, SEEK_SET);
        if (error == SEEK_ERROR) {
            perror("Cannot print line (error while setting file pointer)");
            return EXIT_FAILURE;
        }

        printRequestedAmountOfBytes(filedesc, table[numberOfLine - 1].length);
    } while (numberOfLine != EXIT_CRITERION);

    return EXIT_SUCCESS;
}

int main(int argc, char** argv) {
    if (argc != 2) {
        fprintf(stderr, "One argument (file name) expected\n");
        return EXIT_FAILURE;
    }

    int fileDescriptor = open(argv[1], O_RDONLY);
    if (fileDescriptor == -1) {
        perror("File was not opened");
        return EXIT_SUCCESS;
    }

    int linesInFile, countingError;
    countingError = countLinesInFile(fileDescriptor, &linesInFile);
    if (countingError != EXIT_SUCCESS) {
        close(fileDescriptor);
        return EXIT_FAILURE;
    }

    if (linesInFile == 0) {
        close(fileDescriptor);
        fprintf(stderr, "File is empty\n");
        return EXIT_SUCCESS;
    }

    lineDescription* displacementTable = (lineDescription*)malloc(sizeof(lineDescription) * linesInFile);
    if (displacementTable == NULL) {
        close(fileDescriptor);
        perror("Cannot allocate memory for displacement table");
        return EXIT_FAILURE;
    }

    int buildTableError = buildDisplacementTable(fileDescriptor, displacementTable, linesInFile);
    if (buildTableError != EXIT_SUCCESS) {
        close(fileDescriptor);
        free(displacementTable);
        return EXIT_FAILURE;
    }

    int linePrintingError = printLines(fileDescriptor, displacementTable, linesInFile);
    if (linePrintingError != EXIT_SUCCESS) {
        close(fileDescriptor);
        free(displacementTable);
        return EXIT_FAILURE;
    }

    close(fileDescriptor);
    free(displacementTable);

    return EXIT_SUCCESS;
}
