#include <stdio.h>
#include <stdlib.h>
#include <zconf.h>
#include <sys/wait.h>

#define FORK_ERROR -1
#define WAIT_ERROR -1

#define CHILD_PROCESS 0

#define IDX_OF_FILENAME 1

#define MIN_AMOUNT_OF_ARGUMENTS 2

extern char** environ;

int execvpe(char* file, char** argv, char** envp) {
    char** oldEnviron = environ;
    environ = envp;

    execvp(file, argv);

    environ = oldEnviron;

    return EXIT_FAILURE;
}

int main(int argc, char** argv) {
    if (argc < MIN_AMOUNT_OF_ARGUMENTS) {
        fprintf(stderr, "Expecting at least one argument: name of program\n");
        return EXIT_FAILURE;
    }

    pid_t forkResult = fork();
    if (forkResult == FORK_ERROR) {
        perror("Error while forking process");
        return EXIT_FAILURE;
    }

    char* envp[] = {"PATH=/home/students/2019/y.lyashuk/operating_systems/lab11", NULL};

    if (forkResult == CHILD_PROCESS) {
        execvpe(argv[IDX_OF_FILENAME], &argv[1], envp);
        perror("Exec error");
        return EXIT_FAILURE;
    }

    int waitResult = wait(NULL);
    if (waitResult == WAIT_ERROR) {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}