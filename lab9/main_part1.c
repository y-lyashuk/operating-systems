#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define CHILD_PROCESS 0

#define FORK_ERROR -1

#define IDX_OF_FILENAME 1
#define EXPECTED_AMOUNT_OF_ARGUMENTS 2

int main(int argc, char** argv) {
    if (argc != EXPECTED_AMOUNT_OF_ARGUMENTS) {
        fprintf(stderr, "Error: expecting one argument (name of a long file)\n");
        return EXIT_FAILURE;
    }

    pid_t forkResult = fork();
    if (forkResult == FORK_ERROR) {
        perror("Error while forking process");
        return EXIT_FAILURE;
    }

    if (forkResult == CHILD_PROCESS) {
        execl("/bin/cat", "cat", argv[IDX_OF_FILENAME], NULL);
        perror("Exec error");
        return EXIT_FAILURE;
    }

    printf("** output from parent process **\n");

    return EXIT_SUCCESS;
}